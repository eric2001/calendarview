# CalendarView
![CalendarView](./screenshot.jpg) 

## Description
CalendarView is a module for Gallery 3 which will allow visitors to view a calendar with links leading to the photos that were taken on each day.

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/92405).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "calendarview" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.

## History
**Version 1.7.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Released 24 August 2021.
>
> Download: [Version 1.7.0](/uploads/58f9b6a85db010fbd7aef391df3f4043/calendarview170.zip)

**Version 1.6.1:**
> - Minor display changes for use with Gallery Wind theme.
> - Released 18 June 2012.
>
> Download: [Version 1.6.1](/uploads/59f04bb5e6fc292109011204df33d8a8/calendarview161.zip)

**Version 1.6.0:**
> - Module now displays the last three months on the user profiles page, with a link to view the full year in the CalendarView module.
> - Released 13 June 2012.
>
> Download: [Version 1.6.0](/uploads/627b7b089d221b3bce09fd9eb8eaf97e/calendarview160.zip)

**Version 1.5.0:**
> - Updated to use new virtual album support in Gallery 3.0.3.
> - Released 30 April 2012.
>
> Download: [Version 1.5.0](/uploads/2ea10b834eb59538172b9acc9d07ecc0/calendarview150.zip)

**Version 1.4.2:**
> - Updated module.info file for Gallery 3.0.2 compatibility.
> - Released 15 August 2011.
>
> Download: [Version 1.4.2](/uploads/1755241ba130e615e8de74b4ac40d11b/calendarview142.zip)

**Version 1.4.1:**
> - Bug Fix for displaying years that do not have any photos associated with them.
> - Released 21 April 2011.
>
> Download: [Version 1.4.1](/uploads/8f8849994cce0be43b3ed37511c1b58b/calendarview141.zip)

**Version 1.4.0:**
> - Updated "year view" to use fewer database queries.
> - Updated "EXIF module required" alerts to match current Gallery standards.
> - CSS Bugfix for Gallery 3.0.1.
> - Released 03 March 2011.
>
> Download: [Version 1.4.0](/uploads/3546390d769adec92264a7d7066ba892/calendarview140.zip)

**Version 1.3.2:**
> - Removed background color CSS -- module should look better with third-party themes now.
> - Tweaked the sidebar code to only display links to days and months that have photos taken on them.
> - Released 16 March 2010.
>
> Download: [Version 1.3.2](/uploads/9a5fce92307f102695d8852fdac64453/calendarview132.zip)

**Version 1.3.1:**
> - Bug Fix for users who have mod_rewrite enabled.
> - Released 13 March 2010.
>
> Download: [Version 1.3.1](/uploads/aff1f6eeb3886c35c26638427e744b0d/calendarview131.zip)

**Version 1.3.0:**
> - Make it so month names without any photos aren't clickable.
> - Added title and breadcrumbs to calendar page.
> - CSS Fix for languages that go from right to left
> - Added a sidebar to provide links to other photos that were taken on the same day as the current one.
> - Bug Fix: Filtering CalendarView for a specific user now works again.
> - Released 04 March 2010.
>
> Download: [Version 1.3.0](/uploads/5e52704f7b8a5680fb79d20db43a055e/calendarview130.zip)

**Version 1.2.0:**
> - Added code to display a warning notice if the EXIF module is not installed and active.
> - Re-wrote code responsible for determining month and day names.  Should make translations easier.
> - Made numerous CSS changes to make theming easier.
> - Released 03 March 2010.
>
> Download: [Version 1.2.0](/uploads/aa8a2cd9966685faf88d7994af802e9e/calendarview120.zip)

**Version 1.1.1:**
> - Bug Fix for international translations.
> - Released 01 March 2010.
>
> Download: [Version 1.1.1](/uploads/786ef9930e551e25471fca88d701f204/calendarview111.zip)

**Version 1.1.0:**
> - Updated to work with Gallery 3.0, RC1.
> - Released 24 February 2010.
>
> Download: [Version 1.1.0](/uploads/6273cffe3e24d0698d38fc7f60bfb7e7/calendarview110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 11 November 2009.
>
> Download: [Version 1.0.0](/uploads/958e634360182b3200491078ed07de44/calendarview100.zip)
